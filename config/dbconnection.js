require('dotenv').config();

const userDBSql = process.env.DB_USERNAME;
const passDBSql = process.env.DB_PASSWORD;
const urlDBSql = process.env.DB_URL;
const portDBSql = process.env.DB_PORT;
const nameDBSql = process.env.DB_NAME;
const dialectDBSql = process.env.DB_DIALECT;
const operatorAliasesDBSql = (process.env.DB_OPERATOR_ALIASES == 'true');
const poolMaxDBSql = parseInt(process.env.DB_POOL_MAX);
const poolMinDBSql = parseInt(process.env.DB_POOL_MIN);
const poolAcquireDBSql = parseInt(process.env.DB_POOL_ACQUIRE);
const poolIdleDBSql = parseInt(process.env.DB_POOL_IDLE);
const dbEnv = process.env.NODE_ENV;

const db = {
  userDBSql,
  passDBSql,
  urlDBSql,
  portDBSql,
  nameDBSql,
  dialectDBSql,
  operatorAliasesDBSql,
  poolMaxDBSql,
  poolMinDBSql,
  poolAcquireDBSql,
  poolIdleDBSql,
  dbEnv
};

module.exports = db;

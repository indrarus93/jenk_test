// 'use strict';

// Register supported grant types.
//
// OAuth 2.0 specifies a framework that allows users to grant client
// applications limited access to their protected resources.  It does this
// through a process of the user granting access, and the client exchanging
// the grant for an access token.

const oauth2orize = require('oauth2orize');
const passport    = require('passport');
const validate    = require('./validate');
const oauthOption = require('../../../config/oauthOption')
const db          = require('../../../db')
const {
  LoginService, 
  EngineerLoginService
} = require('../../../app/services/v1');

// create OAuth 2.0 server
const server = oauth2orize.createServer();

// Configured expiresIn
const expiresIn = { expires_in : oauthOption.token.expiresIn };

/**
 * Exchange user id and password for access tokens.
 *
 * The callback accepts the `client`, which is exchanging the user's name and password
 * from the token request for verification. If these values are validated, the
 * application issues an access token on behalf of the user who authorized the code.
 */
server.exchange(oauth2orize.exchange.password( async(client, username, password, scope, done) => {
  try {
    let loginService;
    switch(client.name) {
      case 'Engineer':
        loginService = new EngineerLoginService();
      break;
      case 'User':
        loginService = new LoginService();
      break;
    }
    const user = await loginService.login(username, password);
    const tokens = await validate.generateTokens({ scope, userID: user.Id, userObject: user, clientID: client.id, clientDomain: client.domain });
    if (tokens === false) {
      return done(null, false);
    }
    if (tokens.length === 1) {
      return done(null, tokens[0], null, expiresIn);
    }
    if (tokens.length === 2) {
      return done(null, tokens[0], tokens[1], expiresIn);
    }
    throw new Error('Error exchanging password for tokens'); 
  } 
  catch (error) {
    logger.error(error.stack)
    return done(null, false);
  }
}));

/**
 * Exchange the refresh token for an access token.
 *
 * The callback accepts the `client`, which is exchanging the client's id from the token
 * request for verification.  If this value is validated, the application issues an access
 * token on behalf of the client who authorized the code
 */
server.exchange(oauth2orize.exchange.refreshToken((client, refreshToken, scope, done) => {
  db.refreshTokens.find(refreshToken)
  .then(foundRefreshToken => validate.refreshToken(foundRefreshToken, refreshToken, client))
  .then(foundRefreshToken => validate.generateToken(foundRefreshToken))
  .then(token => done(null, token, null, expiresIn))
  .catch((error) => {
    done(null, false)
    logger.error(error.stack)
  });    
  logger.info(`Access_Token generated for ${client.name}`)  
}));
/**
 * Token endpoint
 *
 * `token` middleware handles client requests to exchange authorization grants
 * for access tokens.  Based on the grant type being exchanged, the above
 * exchange middleware will be invoked to handle the request.  Clients must
 * authenticate when making requests to this endpoint.
 */
exports.token = [
  // passport.authenticate(['basic', 'oauth2-client-password'], { session: false }),
  passport.authenticate(['basic'], { session: false }),
  server.token(),
  server.errorHandler(),
];
const passport = require('./passport');
const oauth2 = require('./oauth2');
const validate = require('./validate');

module.exports = {
  passport,
  oauth2,
  validate
};

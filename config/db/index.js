const srv = require('../../config/serverEnvironment');
const logger = require('../../utils/logger')(__filename, srv.nodeEnv);
const db = require('../../config/dbconnection');
const Sequelize = require('sequelize');

// Setup sequelize
const sequelize = new Sequelize(db.nameDBSql, db.userDBSql, db.passDBSql, {
  host: db.urlDBSql,
  port: db.portDBSql,
  dialect: db.dialectDBSql,
  pool: {
    max: db.poolMaxDBSql,
    min: db.poolMinDBSql,
    acquire: db.poolAcquireDBSql,
    idle: db.poolIdleDBSql
  },
  define: {
    timestamps: false,
    freezeTableName: true,
    paranoid: true,
    omitNull: true
  },
  operatorsAliases: Sequelize.Op,
  logging: logger.debug

});

const database = {};
database.sequelize = sequelize;
database.user = require('../../app/models/user')(sequelize, Sequelize);
database.providerEngineer = require('../../app/models/providerEngineer')(sequelize, Sequelize);

if(db.dbEnv==='development'){
    // models.sequelize.sync();//creates table if they do not already exist
    // models.sequelize.sync({ force: true });//deletes all tables then recreates them useful for testing and development purposes
}

module.exports = database;

require('dotenv').config();

const nodeEnv = process.env.NODE_ENV;
const port = process.env.SERVICE_PORT;

const srv = {
  nodeEnv,
  port
}

module.exports = srv;
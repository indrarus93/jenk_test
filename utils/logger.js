'use strict';

const { createLogger, format, transports } = require('winston');
const { combine, timestamp, printf, colorize, splat } = format;
const fs = require('fs');
const path = require('path');

//const env = process.env.NODE_ENV || 'development';
const logDir = 'logs';

// Create the log directory if it does not exist
// if (!fs.existsSync(logDir)) {
//   fs.mkdirSync(logDir);
// }

const filename = path.join(logDir, 'logfile.log');

const logFormat = printf((info) => {
  if (String(info.level) === 'error') {
    return `${info.timestamp} ${info.label} ${info.level}: ${info.message} : ${info.stack}`;
  }
  else {
    return `${info.timestamp} ${info.label} ${info.level}: ${info.message}`;
  }
});

const logger = (caller, env) => {
  return createLogger({
    // change level if in dev environment versus production
    level: env === 'production' ? 'info' : 'debug',
    format: format.combine(
      format.label({ label: path.basename(caller) }),
      format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
    ),
    transports: [
      new transports.Console({
        // handleExceptions: true,
        format: format.combine(
          // format.colorize(), --> to enable this need to revise the myformat info.level comparison
          logFormat
        )
      }),
      new transports.File({
        handleExceptions: true,
        filename,
        format: format.combine(
          logFormat
        )  
      })   
    ],
    exceptionHandlers: [
      new transports.Console({
        handleExceptions: true,
        format: format.combine(
          logFormat
        )
      }),
    ],
    exitOnError: false
  });
};

module.exports = logger;
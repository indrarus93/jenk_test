
// Catch any exceptions / error
module.exports = function(err, req, res, next) {
  logger.error(err.message, err);
  res.status(500).send('There is an Internal System Error...!');
} 
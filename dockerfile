FROM node:10.15.2
LABEL maintainer="Itommey Dev Team"
LABEL description="BE-MS-Auth for Bribox Authentication services"

ARG NODE_ENV=development
ENV NODE_ENV=${NODE_ENV}

ARG SERVICE_PORT=3001
ENV SERVICE_PORT=${SERVICE_PORT}

# Create app directory
WORKDIR /opt/app

# Install the modules and build the code.
COPY package*.json ./
# RUN npm config set registry http://${NPM_REGISTRY}/ --> no artifactory yet
# RUN WITH_SASL=0 npm install --production --verbose
RUN npm install

# Bundle App Source
COPY . .

# Expose then start the server.
EXPOSE ${SERVICE_PORT}

CMD ["npm","run","start"]

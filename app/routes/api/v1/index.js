const oauth = require('./oauth');
const healthCheck = require('./healthCheck');

module.exports = [
  oauth,
  healthCheck
];

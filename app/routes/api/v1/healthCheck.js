const express = require('express');
const router = express.Router();

const successfulMessages = "Service is healthy";
router.get('/v1/healthCheck', (req, res) => {
    const healthStatus = {
      response_code: "00",
      message: successfulMessages
    }
    res.send(healthStatus);
    logger.info(successfulMessages);
  });

module.exports = router;
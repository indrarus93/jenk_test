const express = require('express');
const router = express.Router();
const oauth2 = require('../../../../../config/oauth/v1/oauth2');
const { createHandler } = require('../../../../../app/handlers/common');
const { v1: {oauth: { refreshTokenHandler: handler } } } = require('../../../../../app/handlers');

/* POST login. */

router.post('/v1/oauth/refreshtoken', createHandler(handler), oauth2.token);

module.exports = router;
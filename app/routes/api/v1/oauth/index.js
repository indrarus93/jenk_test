const token = require('./token');
const refreshToken = require('./refreshToken');

module.exports = [
  token,
  refreshToken
];

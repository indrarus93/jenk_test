
const srv = require('../config/serverEnvironment');
const express = require('express');
const helmet = require('helmet');
const morgan = require('morgan');
const error = require('../middleware/error');
const bodyParser = require('body-parser')
const cookieParser   = require('cookie-parser');
const port = srv.port || 3000;
const cors = require('cors');
const swaggerUi = require('swagger-ui-express');

require('../config/oauth/v1/passport');

/**
 * Application base class
 */
class createExpressApp {

  /**
   * createExpressApp constructor
   * @param {object} opts object for required parameters
   */
  constructor(opts) {
    try {
      Object.assign(this, opts);

      this.app = express();
      this.app.use(cookieParser());

      if (this.corsOptions) {
        this.app.use(cors());
        this.app.options('*', cors());
      }

      if (this.docs) {
        this.app.use('/docs', swaggerUi.serve, swaggerUi.setup(this.docs));
      }

      this.app.use(bodyParser.json());
      this.app.use(express.json());
      this.app.use(bodyParser.urlencoded({
        extended: true
      }));
      this.app.use(helmet());
      
      if (this.logger) {
        this.app.use(morgan('combined', {stream: this.logger.stream}));
      }

      if (this.router) {
        this.app.use('/api', this.router)
      }

      this.app.use(error);
      this.app.listen(port, () => logger.info(`Listening on port ${port}...`));
    } catch (error) {
      logger.error(error.message, error)
    }
    
  }
}

module.exports = createExpressApp;
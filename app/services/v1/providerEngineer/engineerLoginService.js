const models = require('../../../../config/db');

/**
 * Class for engineer login functionality
 * @class EngineerLoginService
 */
class EngineerLoginService {

  /**
   * constructor method
   * @param {object} init - option objects
   */
  constructor() { }
  
  /**
   * Login Engineer.
   * @param {Object} username - Payload of engineer phone number.
   * @param {Object} password - Payload of engineer password.
   * @memberof EngineerLoginService
   */
  async login(username, password) {

    return await models.providerEngineer.findOne({
      where: {mobileNumber: username},
      attributes: ['id', 'name', 'password', 'providerId']
    }).then(results => {
      if(password !== results.password){
        throw new Error(`Password not match for engineer: ${username} `);
      }
      else {
        const finalResults = {
          Id            : results.id,
          name          : results.name,
          providerId    : results.providerId
        }
        logger.info(`Login successful for engineer: ${username}`);;
        logger.debug(JSON.stringify(finalResults, null, 2))
        return finalResults;
      }
    }).catch(() => {throw new Error(`Can't find or verify for engineer: ${username} `)});
  }

}

module.exports = EngineerLoginService;


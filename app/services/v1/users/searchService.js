const srv = require('../../../../config/serverEnvironment');
const logger = require('../../../../utils/logger')(__filename, srv.nodeEnv);
const models = require('../../../../config/db');

/**
 * Class for search functionality
 * @class SearchService
 */
class SearchService {

  /**
   * constructor method
   * @param {object} init - option objects
   */
  constructor() { }
  
  /**
   * Search User by ID.
   * @param {Object} id - Payload of user.id to be searched.
   * @memberof SearchService
   */
  async search(user_id) {

    return await models.user.findOne({
      where: {ID: user_id},
      attributes: ['user']
    }).then(user => {
      return user;
    })    
  }

}

module.exports = SearchService;


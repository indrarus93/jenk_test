const models = require('../../../../config/db');

/**
 * Class for login functionality
 * @class LoginService
 */
class LoginService {

  /**
   * constructor method
   * @param {object} init - option objects
   */
  constructor() { }
  
  /**
   * Login User.
   * @param {Object} username - Payload of user PersonalNumber.
   * @param {Object} password - Payload of user password.
   * @memberof LoginService
   */
  async login(username, password) {

    return await models.user.findOne({
      where: {PERSONAL_NUMBER: username},
      attributes: ['id', 'name', 'password', 'personalNumber', 'title']
    }).then(results => {
      if(password !== results.password){
        throw new Error(`Password not match for user: ${username} `);
      }
      else {
        const finalResults = {
          Id        : results.id,
          name          : results.name,
          personalNumber: results.personalNumber,
          title         : results.title 
        }
        logger.info(`Login successful for user: ${username}`);;
        logger.debug(JSON.stringify(finalResults, null, 2))
        return finalResults;
      }
    }).catch(() => {throw new Error(`Can't find or verify for user: ${username} `)});
  }

}

module.exports = LoginService;


const LoginService = require('./loginService');
const SearchService = require('./searchService');

module.exports = {
  LoginService,
  SearchService
};
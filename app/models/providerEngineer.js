module.exports = (sequelize, Sequelize) => {
	const ProviderEngineer = sequelize.define('Provider_Engineer', {
	  providerId: {
		type: Sequelize.INTEGER,
		field: 'Provider_ID',
			get() {
				const value = this.getDataValue('providerId');
				return (value===null) ? undefined : value;
			}
	  },
		name: {
		type: Sequelize.STRING,
		field: 'Name',
			get() {
				const value = this.getDataValue('name');
				return (value===null) ? undefined : value;
			}
		},
		employeeId: {
		type: Sequelize.STRING,
		field: 'Employee_ID',
			get() {
				const value = this.getDataValue('employeeId');
				return (value===null) ? undefined : value;
			}
	  },
	  phoneNumber: {
		type: Sequelize.STRING,
		field: 'Phone_Number',
			get() {
				const value = this.getDataValue('phoneNumber');
				return (value===null) ? undefined : value;
			}
	  },
    officeEmail: {
		type: Sequelize.STRING,
		field: 'Office_Email',
			get() {
				const value = this.getDataValue('officeEmail');
				return (value===null) ? undefined : value;
			}
	  },
    mobileNumber: {
		type: Sequelize.STRING,
		field: 'Mobile_Number',
			get() {
				const value = this.getDataValue('mobileNumber');
				return (value===null) ? undefined : value;
			}
	  },
    officeExtention: {
		type: Sequelize.STRING,
		field: 'Office_Extention',
			get() {
				const value = this.getDataValue('officeExtention');
				return (value===null) ? undefined : value;
			}
	  },
		activeStatus: {
		type: Sequelize.STRING,
		field: 'Active_Status',
			get() {
				const value = this.getDataValue('activeStatus');
				return (value===null) ? undefined : value;
			}
	  },
		pic: {
		type: Sequelize.STRING,
		field: 'PIC',
			get() {
				const value = this.getDataValue('pic');
				return (value===null) ? undefined : value;
			}
	  },
		password: {
		type: Sequelize.STRING,
		field: 'Password',
			get() {
				const value = this.getDataValue('password');
				return (value===null) ? undefined : value;
			}
	  },
    createDate: {
		type: Sequelize.DATE,
		field: 'Create_Date',
			get() {
				const value = this.getDataValue('createDate');
				return (value===null) ? undefined : value;
			}
	  },
		deletedDate: {
		  type: Sequelize.DATE,
			field: 'Delete_Date',
			get() {
				const value = this.getDataValue('deletedDate');
				return (value===null) ? undefined : value;
			}
	  }
	});
	
	return ProviderEngineer;
}
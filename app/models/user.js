module.exports = (sequelize, Sequelize) => {
	const User = sequelize.define('USER', {
	  user: {
		type: Sequelize.STRING,
		field: 'USER',
			get() {
				const value = this.getDataValue('user');
				return (value===null) ? undefined : value;
			}
	  },
		name: {
		type: Sequelize.STRING,
		field: 'NAME',
			get() {
				const value = this.getDataValue('name');
				return (value===null) ? undefined : value;
			}
		},
		password: {
		type: Sequelize.STRING,
		field: 'PASSWORD',
			get() {
				const value = this.getDataValue('password');
				return (value===null) ? undefined : value;
			}
	  },
	  personalNumber: {
		type: Sequelize.STRING,
		field: 'PERSONAL_NUMBER',
			get() {
				const value = this.getDataValue('personalNumber');
				return (value===null) ? undefined : value;
			}
	  },
    email: {
		type: Sequelize.STRING,
		field: 'EMAIL',
			get() {
				const value = this.getDataValue('email');
				return (value===null) ? undefined : value;
			}
	  },
    handphone: {
		type: Sequelize.STRING,
		field: 'HANDPHONE',
			get() {
				const value = this.getDataValue('handphone');
				return (value===null) ? undefined : value;
			}
	  },
    unitKerjaId: {
		  type: Sequelize.INTEGER,
		field: 'UNIT_KERJA_ID',
			get() {
				const value = this.getDataValue('unitKerjaId');
				return (value===null) ? undefined : value;
			}
	  },
		officeExt: {
		type: Sequelize.STRING,
		field: 'OFFICE_EXT',
			get() {
				const value = this.getDataValue('officeExt');
				return (value===null) ? undefined : value;
			}
	  },
		floor: {
		type: Sequelize.STRING,
		field: 'FLOOR',
			get() {
				const value = this.getDataValue('floor');
				return (value===null) ? undefined : value;
			}
	  },
		title: {
		type: Sequelize.STRING,
		field: 'TITLE',
			get() {
				const value = this.getDataValue('title');
				return (value===null) ? undefined : value;
			}
	  },
    picture: {
		type: Sequelize.STRING,
		field: 'PIC',
			get() {
				const value = this.getDataValue('picture');
				return (value===null) ? undefined : value;
			}
	  },
		deletedDate: {
		  type: Sequelize.DATE,
			field: 'Delete_Date',
			get() {
				const value = this.getDataValue('deletedDate');
				return (value===null) ? undefined : value;
			}
	  }
	});
	
	return User;
}

/**
 * createHandler
 * @param {function} handler - the handler function
 * @returns {function(req, res, next)} - return handler function
 */
const createHandler = (handler) => {
  return async (req, res, next) => {
      return await handler(req, res, next);
  };
};

module.exports = createHandler;


/**
 * Validate Username and Password
 *
 * @param {Object} req: request object
 * @param {Object} res: response object
 * @returns {promise<Object>} promise
 */

const tokenHandler = async (req, res, next) => {
    const user = req.body;
    if(!user.username || user.username.length == 0
      || !user.password || user.password.length == 0)
    {
      logger.error('Username or Password not exist or empty');
      return res.status(400).send('Invalid username or password!');
    }
    await next();
};

module.exports = tokenHandler;
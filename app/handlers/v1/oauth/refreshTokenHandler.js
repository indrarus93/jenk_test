
/**
 * Refresh Access Token
 *
 * @param {Object} req: request object
 * @param {Object} res: response object
 * @returns {promise<Object>} promise
 */

const refreshTokenHandler = async (req, res, next) => {
    const user = req.body;
    if(!user.refresh_token || user.refresh_token.length == 0 
      || !user.grant_type || user.grant_type.length == 0) 
    {
      logger.error('User Grant_Type or Refresh_token not exist or empty');
      return res.status(400).send('Invalid username or password!');
    }    
    await next();
};

module.exports = refreshTokenHandler;
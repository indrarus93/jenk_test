
require('express-async-errors');

const srv = require('./config/serverEnvironment');
const loggerstream = logger = require('./utils/logger')(__filename, srv.nodeEnv);
const YAML = require('yamljs');
const fs = require('fs');
const createExpressApp = require('./app/createExpressApp');
const router = require('./app/routes');

/**
 * create a stream object with a 'write' function that will be used by `morgan`.
 * use the 'info' log level so the output will be picked up by both transports
 */
loggerstream.stream = {
  write: function (message, encoding) {
    loggerstream.info(message);
  }
};

/**
 * create swagger doc for API Documentation.
 */
const swaggerYaml = fs.readFileSync('./docs/swagger.yml', 'utf8');
const swaggerDocument = YAML.parse(swaggerYaml);

/**
 * Setup option for CORS.
 */
// Set up a whitelist and check against it:
var allowedOrigins = ['*'];
var corsOptions = {
  origin: function(origin, callback){
    // allow requests with no origin 
    // (like mobile apps or curl requests)
    if(!origin) return callback(null, true);
    if(allowedOrigins.indexOf(origin) === -1){
      var msg = 'The CORS policy for this site does not ' +
                'allow access from the specified Origin.';
      return callback(new Error(msg), false);
    }
    return callback(null, true);
  },
  optionsSuccessStatus: 200
};


const base = new createExpressApp({
  router,
  docs: swaggerDocument,
  corsOptions,
  logger: loggerstream
});



